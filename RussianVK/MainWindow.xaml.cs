﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RussianVK
{
    /// Ali Reza M
    /// Virtual Keyboard for Russian Language
    /// Dec 3 '16

    public abstract class shiftHandler : MainWindow
    {
        public static bool isShiftPressed = false;
        public static bool isCapsPressed = false;

        public static void shiftIsPressed()
        {
            #region Change the keys content
            _instance.button.Content = "Ё";
            _instance.button_Copy.Content = "!";
            _instance.button_Copy1.Content = "\"";
            _instance.button_Copy2.Content = "№";
            _instance.button_Copy3.Content = ";";
            _instance.button_Copy4.Content = "%";
            _instance.button_Copy5.Content = ":";
            _instance.button_Copy6.Content = "?";
            _instance.button_Copy7.Content = "*";
            _instance.button_Copy8.Content = "(";
            _instance.button_Copy9.Content = ")";
            _instance.button_Copy10.Content = "_";
            _instance.button_Copy11.Content = "+";
            _instance.button_Copy15.Content = "Й";
            _instance.button_Copy16.Content = "Ц";
            _instance.button_Copy17.Content = "У";
            _instance.button_Copy18.Content = "К";
            _instance.button_Copy19.Content = "Е";
            _instance.button_Copy20.Content = "Н";
            _instance.button_Copy21.Content = "Г";
            _instance.button_Copy22.Content = "Ш";
            _instance.button_Copy23.Content = "Щ";
            _instance.button_Copy24.Content = "З";
            _instance.button_Copy25.Content = "Х";
            _instance.button_Copy26.Content = "Ъ";
            _instance.button_Copy27.Content = "Ф";
            _instance.button_Copy28.Content = "/";
            _instance.button_Copy29.Content = "Ы";
            _instance.button_Copy30.Content = "В";
            _instance.button_Copy31.Content = "А";
            _instance.button_Copy32.Content = "П";
            _instance.button_Copy33.Content = "Р";
            _instance.button_Copy34.Content = "О";
            _instance.button_Copy35.Content = "Л";
            _instance.button_Copy36.Content = "Д";
            _instance.button_Copy37.Content = "Ж";
            _instance.button_Copy38.Content = "Э";
            _instance.button_Copy41.Content = "Я";
            _instance.button_Copy42.Content = "Ч";
            _instance.button_Copy43.Content = "С";
            _instance.button_Copy44.Content = "М";
            _instance.button_Copy45.Content = "И";
            _instance.button_Copy46.Content = "Т";
            _instance.button_Copy47.Content = "Ь";
            _instance.button_Copy48.Content = "Б";
            _instance.button_Copy49.Content = "Ю";
            _instance.button_Copy50.Content = ",";
            if (isShiftPressed)
            {
                _instance.button_Copy40.Background = (Brush)_instance.bc.ConvertFrom("#BEE6FD");
                _instance.button_Copy51.Background = (Brush)_instance.bc.ConvertFrom("#BEE6FD");
            }
            else if (isCapsPressed)
            {
                _instance.button_Copy12.Background = (Brush)_instance.bc.ConvertFrom("#BEE6FD");
            }
            #endregion
        }
        public static void shiftNotPressed()
        {
            #region Change back the keys content
            _instance.button.Content = "ё";
            _instance.button_Copy.Content = "1";
            _instance.button_Copy1.Content = "2";
            _instance.button_Copy2.Content = "3";
            _instance.button_Copy3.Content = "4";
            _instance.button_Copy4.Content = "5";
            _instance.button_Copy5.Content = "6";
            _instance.button_Copy6.Content = "7";
            _instance.button_Copy7.Content = "8";
            _instance.button_Copy8.Content = "9";
            _instance.button_Copy9.Content = "0";
            _instance.button_Copy10.Content = "-";
            _instance.button_Copy11.Content = "=";
            _instance.button_Copy15.Content = "й";
            _instance.button_Copy16.Content = "ц";
            _instance.button_Copy17.Content = "у";
            _instance.button_Copy18.Content = "к";
            _instance.button_Copy19.Content = "е";
            _instance.button_Copy20.Content = "н";
            _instance.button_Copy21.Content = "г";
            _instance.button_Copy22.Content = "ш";
            _instance.button_Copy23.Content = "щ";
            _instance.button_Copy24.Content = "з";
            _instance.button_Copy25.Content = "х";
            _instance.button_Copy26.Content = "ъ";
            _instance.button_Copy27.Content = "ф";
            _instance.button_Copy28.Content = "\\";
            _instance.button_Copy29.Content = "ы";
            _instance.button_Copy30.Content = "в";
            _instance.button_Copy31.Content = "а";
            _instance.button_Copy32.Content = "п";
            _instance.button_Copy33.Content = "р";
            _instance.button_Copy34.Content = "о";
            _instance.button_Copy35.Content = "л";
            _instance.button_Copy36.Content = "д";
            _instance.button_Copy37.Content = "ж";
            _instance.button_Copy38.Content = "э";
            _instance.button_Copy41.Content = "я";
            _instance.button_Copy42.Content = "ч";
            _instance.button_Copy43.Content = "с";
            _instance.button_Copy44.Content = "м";
            _instance.button_Copy45.Content = "и";
            _instance.button_Copy46.Content = "т";
            _instance.button_Copy47.Content = "ь";
            _instance.button_Copy48.Content = "б";
            _instance.button_Copy49.Content = "ю";
            _instance.button_Copy50.Content = ".";
            if (!isShiftPressed)
            {
                if (Properties.Settings.Default.themeIsLight)
                {
                    _instance.button_Copy40.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
                    _instance.button_Copy51.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
                }
                else
                {
                    _instance.button_Copy40.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
                    _instance.button_Copy51.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
                }
            }
            if (!isCapsPressed)
            {
                if (Properties.Settings.Default.themeIsLight)
                {
                    _instance.button_Copy12.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
                }
                else
                {
                    _instance.button_Copy12.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
                }
            }
            #endregion
        }
    }

    public partial class MainWindow : Window
    {
        public BrushConverter bc = new BrushConverter();
        public static MainWindow _instance;

        public MainWindow()
        {
            InitializeComponent();
            _instance = this;
            if (Properties.Settings.Default.themeIsLight)
            {
                themeLight();
            }
            else
            {
                themeDark();
            }
        }
        public static void themeLight()
        {
            Properties.Settings.Default.themeIsLight = true;
            Properties.Settings.Default.Save();

            #region Light Backgrounds
            _instance.button.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy1.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy2.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy3.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy4.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy5.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy6.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy7.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy8.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy9.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy10.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy11.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy12.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy13.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy14.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy15.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy16.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy17.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy18.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy19.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy20.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy21.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy22.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy23.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy24.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy25.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy26.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy27.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy28.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy29.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy30.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy31.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy32.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy33.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy34.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy35.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy36.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy37.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy38.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy39.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy40.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy41.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy42.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy43.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy44.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy45.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy46.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy47.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy48.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy49.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy50.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy51.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy52.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy53.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy54.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy55.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy56.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy57.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy58.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy59.Background = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.backG.Background = SystemColors.ControlLightLightBrush;
            _instance.textBox.Background = (Brush)_instance.bc.ConvertFrom("#FFFFFFFF");
            #endregion

            #region Light Forgrounds
            _instance.button.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy1.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy2.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy3.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy4.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy5.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy6.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy7.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy8.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy9.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy10.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy11.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy12.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy13.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy14.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy15.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy16.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy17.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy18.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy19.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy20.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy21.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy22.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy23.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy24.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy25.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy26.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy27.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy28.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy29.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy30.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy31.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy32.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy33.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy34.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy35.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy36.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy37.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy38.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy39.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy40.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy41.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy42.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy43.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy44.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy45.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy46.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy47.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy48.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy49.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy50.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy51.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy52.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy53.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy54.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy55.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy56.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy57.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy58.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.button_Copy59.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.textBox.Foreground = (Brush)_instance.bc.ConvertFrom("#000000");
            #endregion
        }
        public static void themeDark()
        {
            Properties.Settings.Default.themeIsLight = false;
            Properties.Settings.Default.Save();

            #region Dark Backgournds
            if (_instance.button.IsMouseOver)
            {
                _instance.button.Background = (Brush)_instance.bc.ConvertFrom("##FFE42626");
            }
            _instance.button.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy1.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy2.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy3.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy4.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy5.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy6.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy7.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy8.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy9.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy10.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy11.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy12.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy13.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy14.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy15.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy16.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy17.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy18.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy19.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy20.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy21.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy22.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy23.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy24.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy25.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy26.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy27.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy28.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy29.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy30.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy31.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy32.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy33.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy34.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy35.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy36.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy37.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy38.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy39.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy40.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy41.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy42.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy43.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy44.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy45.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy46.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy47.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy48.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy49.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy50.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy51.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy52.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy53.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy54.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy55.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy56.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy57.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy58.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.button_Copy59.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            _instance.backG.Background = (Brush)_instance.bc.ConvertFrom("#000000");
            _instance.textBox.Background = (Brush)_instance.bc.ConvertFrom("#FF292929");
            #endregion

            #region Dark Forgrounds
            _instance.button.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy1.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy2.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy3.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy4.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy5.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy6.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy7.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy8.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy9.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy10.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy11.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy12.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy13.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy14.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy15.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy16.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy17.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy18.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy19.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy20.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy21.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy22.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy23.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy24.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy25.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy26.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy27.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy28.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy29.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy30.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy31.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy32.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy33.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy34.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy35.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy36.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy37.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy38.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy39.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy40.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy41.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy42.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy43.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy44.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy45.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy46.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy47.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy48.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy49.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy50.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy51.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy52.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy53.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy54.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy55.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy56.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy57.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy58.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.button_Copy59.Foreground = (Brush)_instance.bc.ConvertFrom("#FFDDDDDD");
            _instance.textBox.Foreground = (Brush)_instance.bc.ConvertFrom("#FFFFFFFF");

            #endregion
        }

        #region writing buttons

        private void button_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy1_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy1.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy2_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy2.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy3_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy3.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy4_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy4.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy5_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy5.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy6_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy6.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy7_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy7.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy8_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy8.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy9_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy9.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy10_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy10.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy11_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy11.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy14_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + "\t";
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy15_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy15.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy16_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy16.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy17_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy17.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy18_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy18.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy19_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy19.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy20_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy20.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy21_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy21.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy22_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy22.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy23_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy23.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy24_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy24.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy25_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy25.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy26_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy26.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy28_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy28.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy27_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy27.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy29_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy29.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy30_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy30.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy31_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy31.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy32_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy32.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy33_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy33.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy34_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy34.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy35_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy35.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy36_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy36.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy37_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy37.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy38_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy38.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy41_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy41.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy42_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy42.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy43_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy43.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy44_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy44.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy45_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy45.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy46_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy46.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy47_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy47.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy48_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy48.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy49_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy49.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy50_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + _instance.button_Copy50.Content;
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy55_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + ' ';
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy39_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + "\n";
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
                shiftHandler.shiftNotPressed();
            }
        }

        #endregion

        #region functional buttons

        private void button_Copy13_Click(object sender, RoutedEventArgs e)
        {
            if (textBox.Text.Length > 0)
            {
                textBox.Text = textBox.Text.Substring(0, textBox.Text.Length - 1);
            }
        }

        private void button_Copy58_Click(object sender, RoutedEventArgs e)
        {
            ThemeSelector newThemer = new ThemeSelector();
            newThemer.ShowDialog();
        }

        private void button_Copy52_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.Clipboard.SetText(textBox.Text);
            }
            catch
            {
                //Do nothing
            }
            textBox.SelectAll();
            textBox.Focus();
        }

        private void button_Copy59_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.Clipboard.SetText(textBox.Text);
            }
            catch
            {
                //Do nothing
            }
            textBox.Clear();
        }

        private void button_Copy40_Click(object sender, RoutedEventArgs e)
        {
            shiftHandler.isCapsPressed = false;
            shiftHandler.shiftNotPressed();
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
            }
            else
            {
                shiftHandler.isShiftPressed = true;
            }

            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.shiftIsPressed();
            }
            else
            {
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy51_Click(object sender, RoutedEventArgs e)
        {
            shiftHandler.isCapsPressed = false;
            shiftHandler.shiftNotPressed();
            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.isShiftPressed = false;
            }
            else
            {
                shiftHandler.isShiftPressed = true;
            }

            if (shiftHandler.isShiftPressed)
            {
                shiftHandler.shiftIsPressed();
            }
            else
            {
                shiftHandler.shiftNotPressed();
            }
        }

        private void button_Copy57_Click(object sender, RoutedEventArgs e)
        {
            AboutBox about = new AboutBox();
            about.ShowDialog();
        }

        private void button_Copy53_Click(object sender, RoutedEventArgs e)
        {
            AboutBox about = new AboutBox();
            about.ShowDialog();
        }

        private void button_Copy56_Click(object sender, RoutedEventArgs e)
        {
            textBox.Clear();
        }
        private void button_Copy54_Click(object sender, RoutedEventArgs e)
        {
            textBox.Clear();
        }

        #endregion

        private void button_Copy12_Click(object sender, RoutedEventArgs e)
        {
            shiftHandler.isShiftPressed = false;
            shiftHandler.shiftNotPressed();
            if (shiftHandler.isCapsPressed)
            {
                shiftHandler.isCapsPressed = false;
            }
            else
            {
                shiftHandler.isCapsPressed = true;
            }

            if (shiftHandler.isCapsPressed)
            {
                shiftHandler.shiftIsPressed();
            }
            else
            {
                shiftHandler.shiftNotPressed();
            }
        }
    }
}
