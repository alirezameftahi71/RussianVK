﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RussianVK
{
    /// <summary>
    /// Interaction logic for ThemeSelector.xaml
    /// </summary>
    public partial class ThemeSelector : Window
    {
        public ThemeSelector()
        {
            InitializeComponent();
            if (Properties.Settings.Default.themeIsLight)
            {
                rd1.IsChecked = true;
            }
            else
            {
                rd2.IsChecked = true;
            }
        }

        private void rd2_Checked(object sender, RoutedEventArgs e)
        {
            MainWindow.themeDark();
        }

        private void rd1_Checked(object sender, RoutedEventArgs e)
        {
            MainWindow.themeLight();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
